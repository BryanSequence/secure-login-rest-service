package com.bryanjur.authentication.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bryanjur.authentication.model.User;

@Repository
public interface UserDao extends CrudRepository<User, Long> {
	
	public User save(User user);
	
	public User findByEmail(String email);

}
