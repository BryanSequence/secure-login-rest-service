package com.bryanjur.authentication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bryanjur.authentication.model.User;
import com.bryanjur.authentication.service.Password;
import com.bryanjur.authentication.service.UserService;


@RestController
@RequestMapping("/secure")
public class RegistrationController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/user/users")
	public String loginSuccess() {
		return "Login Successful!";
	}

	@RequestMapping(value = "/user/email", method = RequestMethod.POST)
	public User findByEmail(@RequestBody String email) {
		return userService.findByEmail(email);
	}

	@RequestMapping(value = "/user/update", method = RequestMethod.POST)
	public User updateUser(@RequestBody User user) {
		Password pw = new Password();
		String plain_pwd = user.getPassword();
		
		try {
			user.setPassword(pw.getSaltedHash(plain_pwd));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return userService.save(user);
	}
}
