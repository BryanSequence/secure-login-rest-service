package com.bryanjur.authentication.service;

import com.bryanjur.authentication.model.User;

public interface UserService {
	public User save(User user);
	public User findByEmail(String email);
}
